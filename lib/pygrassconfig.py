#!/usr/bin/python
# -*- coding: UTF-8 -*-
'''
Author: volker@geodatenmanufaktur.de
Purpose: config file for pygrass.py
'''

# GRASS 7 Path Windows
grass7bin_win = r'C:\OSGeo4W\bin\grass70svn.bat'
# GRASS 7 Path Linux - we assume that the GRASS GIS start script is available and in the PATH
grass7bin_lin = 'grass70'
