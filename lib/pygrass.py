#!/usr/bin/python
# -*- coding: UTF-8 -*-
'''
Author: volker@geodatenmanufaktur.de

Purpose: A Python script to generate a new GRASS GIS 7 location simply from metadata in order to use grass scripting without starting GRASS explicitly ... though GRASS 7 needs to be installed.

Todo: - remove obsolete code: search for "NEEDED?"
      - grass.script.setup.init() ... should it also be closed?
      - test for windows installations


Based on: https://grasswiki.osgeo.org/wiki/Working_with_GRASS_without_starting_it_explicitly
by Markus Neteler, 2014

Permission is granted to copy, distribute and/or modify this document under the terms of the GNU Free Documentation License, Version 1.3.
'''

import os
import sys
import subprocess
import shutil
import binascii
import tempfile
import pygrassconfig

# Get GRASS 7 Path
grass7bin_win = pygrassconfig.grass7bin_win
grass7bin_lin = pygrassconfig.grass7bin_lin


class GrassSession:
    '''Class to start a GRASS Session with a temporary mapset. GRASS 7 needs to be installed grass7bin path needs to be set in pygrass-config.py.
    Input: EPSG code of your mapset
    Output: None'''

    def __init__(self, epsg):
        self.location_path = ""
        self.gscript = ""
        self.gsetup = ""
        self.mapset = ""
        epsg = str(epsg)

        # detect OS
        if sys.platform.startswith('linux'):
            grass7bin = grass7bin_lin
        elif sys.platform.startswith('win'):
            grass7bin = grass7bin_win
        else:
            OSError('Operating System not supported.')

        # call GRASS7
        startcmd = grass7bin + ' --config path'

        p = subprocess.Popen(startcmd, shell=True,
                             stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = p.communicate()

        if p.returncode != 0:
            print >>sys.stderr, 'ERROR: %s' % err
            print >>sys.stderr, "ERROR: Cannot find GRASS GIS 7 start script (%s)" % startcmd
            sys.exit(-1)

        # find GISBASE
        if sys.platform.startswith('linux'):
            gisbase = out.strip('\n')
        elif sys.platform.startswith('win'):
            if out.find("OSGEO4W home is") != -1:
                gisbase = out.strip().split('\n')[1]
            else:
                gisbase = out.strip('\n')
            os.environ['GRASS_SH'] = os.path.join(gisbase, 'msys', 'bin', 'sh.exe')

        # Set GISBASE environment variable
        os.environ['GISBASE'] = gisbase

        # define GRASS-Python environment
        gpydir = os.path.join(gisbase, "etc", "python")
        sys.path.append(gpydir)

        # define GRASS DATABASE
        if sys.platform.startswith('win'):
            gisdb = os.path.join(os.getenv('APPDATA', 'grassdata'))
        else:
            gisdb = os.path.join(os.getenv('HOME', 'grassdata'))

        # override for now with TEMP dir
        gisdb = os.path.join(tempfile.gettempdir(), 'grassdata')
        try:
            os.stat(gisdb)
        except:
            os.mkdir(gisdb)

        # location/mapset: use random names for batch jobs
        string_length = 16
        location = binascii.hexlify(os.urandom(string_length))
        mapset   = 'PERMANENT'
        self.location_path = os.path.join(gisdb, location)

        # Create new location with EPSG code:
        startcmd = grass7bin + ' -c epsg:' + epsg + ' -e ' + self.location_path

        print startcmd
        p = subprocess.Popen(startcmd, shell=True,
                             stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = p.communicate()
        if p.returncode != 0:
            print >>sys.stderr, 'ERROR: %s' % err
            print >>sys.stderr, 'ERROR: Cannot generate location (%s)' % startcmd
            sys.exit(-1)
        else:
            print 'Created location %s' % self.location_path

        # Now the location with PERMANENT mapset exists.

        # Set GISDBASE environment variable
        os.environ['GISDBASE'] = gisdb

        # Linux: Set path to GRASS libs (TODO: NEEDED?)
        path = os.getenv('LD_LIBRARY_PATH')
        gbdir  = os.path.join(gisbase, 'lib')
        if path:
            path = gbdir + os.pathsep + path
        else:
            path = gbdir
        os.environ['LD_LIBRARY_PATH'] = path

        # language; NEEDED?
        # os.environ['LANG'] = 'en_US'
        # os.environ['LOCALE'] = 'C'

        # Windows: NEEDED?
        #path = os.getenv('PYTHONPATH')
        #dirr = os.path.join(gisbase, 'etc', 'python')
        #if path:
        #    path = dirr + os.pathsep + path
        #else:
        #    path = dirr
        #os.environ['PYTHONPATH'] = path

        ## Import GRASS Python bindings
        import grass.script as grass
        import grass.script.setup as gsetup

        # Launch session and do something
        gsetup.init(gisbase, gisdb, location, mapset)

        # say hello
        grass.message('--- GRASS GIS 7: Current GRASS GIS 7 environment:')
        print grass.gisenv()

        self.gscript = grass
        self.gsetup = gsetup
        self.mapset = mapset


    def close(self):
        '''Finally remove the temporary batch location from disk'''
        print 'Removing location %s' % self.location_path
        shutil.rmtree(self.location_path)
        sys.exit(0)


# test = GrassSession(4326)
# test.close()
