#!/usr/bin/python
# -*- coding: UTF-8 -*-
'''
Author: volker@geodatenmanufaktur.de

Purpose: A Python script to generate a new GRASS GIS 7 location simply from metadata in order to use grass scripting without starting GRASS explicitly ... though GRASS 7 needs to be installed.

Based on: https://grasswiki.osgeo.org/wiki/Working_with_GRASS_without_starting_it_explicitly
by Markus Neteler, 2014
'''

from lib import pygrass

MyGrassSession = pygrass.GrassSession(4326)

MyGrassSession.gscript.run_command('v.in.ogr', input="sampledata.geojson", output="sampledata", layer="OGRGeoJSON", flags="e", quiet="True")

MyGrassSession.gscript.message('Available vector maps:')
MyGrassSession.gscript.run_command('g.list', type='vector')
MyGrassSession.gscript.run_command('v.info', map='sampledata@PERMANENT')  

MyGrassSession.close()
