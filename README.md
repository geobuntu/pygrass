# README #

* Version 0.1

### What is this repository for? ###

* pygrass provides a temporary GRASS GIS 7 location as a module which can be used easier then the example from https://grasswiki.osgeo.org/wiki/Working_with_GRASS_without_starting_it_explicitly
* allows you to use grass functions from python modules without starting grass

### How do I get set up? ###

* needs Grass 7 installed on your box
* path to grass7bin needs to be set in pygrass.py according to your installation 
* apart from that like regular custom python modules, see example-usage.py

### Who do I talk to? ###

* volker@geodatenmanufaktur.de

### License ###

Permission is granted to copy, distribute and/or modify this document under the terms of the GNU Free Documentation License, Version 1.3.